package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@RestController
@Controller
public class HelloController {

	/*
    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        style += "body { background-color: green; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";
        style += "</style>";
        
        String message = "Hello World!!";
        
        String body = "<body>" + message + "</body>";

        printMessage();

        return style + body;
    }

    private void printMessage() {
        try {
            System.out.println("Hello world!");
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    */
    @GetMapping({"/"})
    public String home() {
        return "home";
    }

    @GetMapping({"/hello"})
    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
    	model.addAttribute("name", name);
    	return "hello";
    }
   

}
